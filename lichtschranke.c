/** @mainpage Lichschranke 
 *  
 *  @section sec1 Einleitung 
 *  Bei dem nachfolgendem Programm geht es um die Zeiterfassung
 *  eines Kanuslalom Rennens mit Hilfe zweier Lichtschranken. Die Signale
 *  der Lichtschranken werden mit einem Mikrocontroller von Typ Atmel32
 *  ausgewertet und die gemessenen Zeiten ueber eine Serielle Schnittstelle
 *  an einen PC ausgegeben. Die Aufloesung der Anlage liegt bei einer 1/100 Sekunde
 * 
 *  @author Klaus Bluemel
 *  @date 21.09.2015 
 * 
 *  @version 1.0.2
 *
 *  @section sec2 Hosting 
 *  Der Sourcecode ist gehostet bei bitbucket.org und frei verfuegbar
 *  https://kbluemel@bitbucket.org/kbluemel/lichtschranke.git
 *  @todo Start und Ziel in einer Zeile ausgeben
 *
 *  @todo Startnummer und Startzeit in ein Array speichern. Bei der Zieldurchfahrt die 
 *  Startzeit wieder aus dem Array auslesen und von der Zielzeit abziehen und als Fahrzeit ausgeben.
 *  @todo Delay von 5 Sekunden fuer die Sperrung der Startzeiten einpflegen
 *
 *  @todo Einstellbares Delay fuer die Unterdrueckung der Zielzeiten einpflegen
 *
 *  @todo Ueberpruefen ob die Interrupts ueberhaupt ausgewertet werden. Das automatische
 *  hochzaehlen der Start und Zielnummern in den Interruproutinen funktioniert
 *  naemlich nicht.
 *  @todo Fehlerhafte Eingaben fuer Start und Zielnummern abfangen
 */


 
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <avr/eeprom.h>
#include <stdlib.h>
#include <string.h>

#define UART_MAXSTRLEN 10
/** Dauer des Signals im Ziel in hundertstel Sekunden */
#define LAUTSPRECHER_DAUER_ZIEL 5
/** Dauer des Signals am Start in hundertstel Sekunden */
#define LAUTSPRECHER_DAUER_START 5
/** Baudrate der seriellen Schnittstelle */
#define BAUD 9600UL      // Baudrate  
#define SIGNALGEBER_START_EINSCHALTEN() PORTD &=~(1<<PD6 )
#define SIGNALGEBER_START_AUSSCHALTEN() PORTD |=(1<<PD6 )
#define SIGNALGEBER_ZIEL_EINSCHALTEN()  PORTD &=~(1<<PD7 )
#define SIGNALGEBER_ZIEL_AUSSCHALTEN()  PORTD |=(1<<PD7 )
#define INTERRUPT_START_ERKANNT 0x01
#define INTERRUPT_ZIEL_ERKANNT 0x02
#define DELAY_ZIEL_ERKANNT 0x04
#define ZIEL_DELAY 3
#define START_DELAY 300
#define UBRR_VAL ((F_CPU+BAUD*8)/(BAUD*16)-1)   // clever runden                     
#define BAUD_REAL (F_CPU/(16*(UBRR_VAL+1)))     // Reale Baudrate                    
#define BAUD_ERROR ((BAUD_REAL*1000)/BAUD) // Fehler in Promille, 1000 = kein Fehler.
unsigned int gui_Status;
unsigned int startnummer; 

//Variablen fuer die Zeit
/** wird von der Funktion ISR(TIMER0_COMP_vect) alle hundertstel Sekunden hochgezaehlt */
volatile unsigned long vgul_HundertstelSekunden; 
 
/**  ul_EndzeitpunktZielsignal definiert den Zeitpunkt bis zu dem die Signalhupe im Ziel mindestens ertoent */                                           
unsigned long ul_EndzeitpunktZielsignal;             
/**  ul_EnzeitpunktStartsignal definiert den Zeitpunkt bis zu dem die Signalhupe am Start mindestens ertoent */
unsigned long ul_EndzeitpunktStartsignal;
										
/** Hier wird die gemessene Startzeit gespeichert von ISR(INT0_vect) */
volatile unsigned long vgul_StartHundertstelsekunden;
/** Hier wird die gemessene Zielzeit gespeichert von ISR(INT1_vect) */
volatile unsigned long vgul_ZielHundertstelsekunden;

/** Variablen fuer die UART Interruptroutine. Wird 1 wenn der String komplett empfangen ist */
volatile uint8_t uart_str_complete = 0;  
volatile uint8_t uart_str_count = 0;
char uart_string[UART_MAXSTRLEN + 1] = ""; 
char tmp_offset[UART_MAXSTRLEN + 1] = ""; 
char erstes_zeichen[UART_MAXSTRLEN + 1] = ""; 
char zielnummer_ausgabe[UART_MAXSTRLEN + 1] = ""; 
char startnummer_ausgabe[UART_MAXSTRLEN + 1] = "";

char gc_AusgabeString[36];

/** In neueren Version der WinAVR/Mfile Makefile-Vorlage kann                         
   F_CPU im Makefile definiert werden, eine nochmalige Definition                    
   hier wuerde zu einer Compilerwarnung fuehren. Daher "Schutz" durch                
   #ifndef/#endif                                                                    
                                                                                     
   Dieser "Schutz" kann zu Debugsessions fuehren, wenn AVRStudio                      
   verwendet wird und dort eine andere, nicht zur Hardware passende                  
   Taktrate eingestellt ist: Dann wird die folgende Definition                       
   nicht verwendet, sondern stattdessen der Defaultwert (8 MHz)                     
   von AVRStudio - daher Ausgabe einer Warnung falls F_CPU                           
   noch nicht definiert: */ 
#ifndef F_CPU 
                                                                       
/**warning "F_CPU war noch nicht definiert, wird nun nachgeholt mit 14745600" 
* Systemtakt in Hz - Definition als unsigned long beachten  
* Ohne ergeben sich unten Fehler in der Berechnung   
*/     
#define F_CPU 14745600UL  
#endif                                                                                     

//#define F_CPU 14745600  wird im Makefile definiert 

/**
  * \author Klaus Bluemel
  * \brief  ISR(TIMER0_COMP_vect)
  *  der Prescaler ist auf 1024 eingestellt. Der Compare Interrupt Handler wird aufgerufen, wenn
  *  TCNT0 = OCR0A = 144-1  ist (144 Schritte), d.h. genau alle 10 ms    
  *  Dadurch ergibt sich bei einer Quarzfrequenz von 14745600 HZ 
  * \f$ 14745600 HZ / 1024*144 = 100 HZ. \f$
  *  Dadurch wird 100 mal pro Sekunde ein Interrupt ausgeloest. 
  */
ISR(TIMER0_COMP_vect) {
	vgul_HundertstelSekunden++;  /** Die globale Variable vgul_HundertstelSekunden wird um 1 erhoeht */
	
}

/**
  * \author Klaus Bluemel
  * \brief  ISR(INT0_vect)
  * Bei Ausloesung des Interrupt wird die aktuelle Zeit in der Variablen 
  * vgul_StartHundertstelsekunden abgespeichert 
  */
ISR(INT0_vect)                 // Messwert abfragen
{
	vgul_StartHundertstelsekunden = vgul_HundertstelSekunden;
//	cli();
	GICR &= (1 << INT0 | 1 << INT1); // INT0 und INT1 deaktivieren
	gui_Status|=INTERRUPT_START_ERKANNT;
}

/**
  * \author Klaus Bluemel
  * \brief  ISR(INT1_vect)
  * Bei Ausloesung des Interrupt wird die aktuelle Zeit in der Variablen 
  * vgul_ZielHundertstelsekunden abgespeichert 
  */
ISR(INT1_vect)                 // Messwert abfragen
{
	vgul_ZielHundertstelsekunden = vgul_HundertstelSekunden;
	GICR &= (1 << INT0 | 1 << INT1); // INT0 und INT1 deaktivieren
//	cli();
	gui_Status|=INTERRUPT_ZIEL_ERKANNT;
}

ISR(USART_RXC_vect)
{
  unsigned char nextChar;

  // Daten aus dem Puffer lesen
  nextChar = UDR;
  if( uart_str_complete == 0 ) {	// wenn uart_string gerade in Verwendung, neues Zeichen verwerfen
 
    // Daten werden erst in uart_string geschrieben, wenn nicht String-Ende/max Zeichenlaenge erreicht ist/string gerade verarbeitet wird
    if( nextChar != '\n' &&
        nextChar != '\r' &&
        uart_str_count < UART_MAXSTRLEN ) {
      uart_string[uart_str_count] = nextChar;
      uart_str_count++;
    }
    else {
      uart_string[uart_str_count] = '\0';
      uart_str_count = 0;
      uart_str_complete = 1;
	  
    }
  }
}

/**
  * \author Klaus Bluemel
  * \brief  uart_init(void)
  * Die Register fuer die serielle Schnittstelle initialisieren
  */
void uart_init(void)                                                                                                                
{                                                             
  UBRRH = UBRR_VAL >> 8;                                      
  UBRRL = UBRR_VAL & 0xFF;         
  /** UART TX RX und Inetrrupt einschalten */
  UCSRB |= (1<<TXEN)| (1<<RXEN) |(1<<RXCIE);  
  /** Asynchron 8N1 */
  UCSRC = (1<<URSEL)|(1<<UCSZ1)|(1<<UCSZ0);  
}                                                             
     
/* ATmega32 */
int uart_putc(unsigned char c)
	{
    while (!(UCSRA & (1<<UDRE)))  /* warten bis Senden moeglich */
		{
		}                             
 
    UDR = c;                      /* sende Zeichen */
    return 0;
	}
 
/* puts ist unabhaengig vom Controllertyp */
void uart_puts (char *s)
	{
    while (*s)
		{   /* so lange *s != '\0' also ungleich dem "String-Endezeichen(Terminator)" */
        uart_putc(*s);
        s++;
		}
	} 

/* Zeichen empfangen */
uint8_t uart_getc(void)
{
    while (!(UCSRA & (1<<RXC)))   // warten bis Zeichen verfuegbar
        ;
    return UDR;                   // Zeichen aus UDR an Aufrufer zurueckgeben
} 

void uart_gets( char* Buffer, uint8_t MaxLen )
{
  uint8_t NextChar;
  uint8_t StringLen = 0;
 
  NextChar = uart_getc();         // Warte auf und empfange das naechste Zeichen
 
                                  // Sammle solange Zeichen, bis:
                                  // * entweder das String Ende Zeichen kam
                                  // * oder das aufnehmende Array voll ist
  while( NextChar != '\n' && StringLen < MaxLen - 1 ) {
    *Buffer++ = NextChar;
    StringLen++;
    NextChar = uart_getc();
  }
 
                                  // Noch ein '\0' anhaengen um einen Standard
                                  // C-String daraus zu machen
  *Buffer = '\0';
} 

/**
  * \author Klaus Bluemel
  * \brief  zeit_ausgeben
  *
  *         Diese Funktion rechnet den uebergebenen Zeitwert in
  *         Stunden, Minuten, Sekunden und Hundertstel Sekunden um
  *			und erzeugt einen String im Format 13:04:05:75. Der in char 
  *			uebergebene Sting wird vorab ausgegeben
  *
  * \param[in]      zeitwert (in hundertstel Sekunden) 
  * \param[in] 		start_oder_ziel (text welcher mit ausgegeben werden soll)
  * \return     String
  * \todo       Die Variablen sind global definiert. Das soll geaendert werden und 
  * die Varaiblen in der Funktion deklariert werden.  
  */
void zeit_ausgeben(unsigned long ul_zeitwert, char *pc_start_oder_ziel)
{	
	/** temporaerer Zeitwert als String */
	char c_TemporaererZeitwertAlsString[2];
	/** temporaerer Zeitwert als numerisch */
	int ui_TemporaererZeitwertNumerisch;
	/** Ausgabestring */ 
	//char out[36]; 

	/** Hundertstel Sekunken gesamt */
	// ltoa(zeitwert, _hundertstelsekunden, 10);	
	strcat(gc_AusgabeString, " "); 
 	strcat(gc_AusgabeString, pc_start_oder_ziel);		
	strcat(gc_AusgabeString, " ");
	
	/** Stunden */
	ui_TemporaererZeitwertNumerisch= ul_zeitwert/360000 % 24;
	itoa(ui_TemporaererZeitwertNumerisch, c_TemporaererZeitwertAlsString, 10);
	if (strlen(c_TemporaererZeitwertAlsString) < 2) {
		strcat(gc_AusgabeString, "0");
	}
	strcat(gc_AusgabeString, c_TemporaererZeitwertAlsString);
	strcat(gc_AusgabeString, ":");
						
	/** Minuten */			
	ui_TemporaererZeitwertNumerisch= ul_zeitwert/6000  %60 ;
	itoa(ui_TemporaererZeitwertNumerisch, c_TemporaererZeitwertAlsString, 10);
	if (strlen(c_TemporaererZeitwertAlsString) < 2) 
	{
		strcat(gc_AusgabeString, "0");
	}
	strcat(gc_AusgabeString, c_TemporaererZeitwertAlsString);			
	strcat(gc_AusgabeString, ":");
						
	/** Sekunden */			
	ui_TemporaererZeitwertNumerisch=ul_zeitwert / 100 % 60;
	itoa(ui_TemporaererZeitwertNumerisch, c_TemporaererZeitwertAlsString, 10);
	if (strlen(c_TemporaererZeitwertAlsString) < 2) 
	{
		strcat(gc_AusgabeString, "0");
	}
	strcat(gc_AusgabeString, c_TemporaererZeitwertAlsString);			
	strcat(gc_AusgabeString, ":");
						
	/** Hundertstel Sekunden */			
	ui_TemporaererZeitwertNumerisch= ul_zeitwert % 100;
	itoa(ui_TemporaererZeitwertNumerisch, c_TemporaererZeitwertAlsString, 10);
	if (strlen(c_TemporaererZeitwertAlsString) < 2) 
	{
		strcat(gc_AusgabeString, "0");
	}
	strcat(gc_AusgabeString, c_TemporaererZeitwertAlsString);
	strcat(gc_AusgabeString, "\r\n");
			
	/** Ausgabe an die serielle Schnittstelle  */
	uart_puts(gc_AusgabeString);
	strcpy(gc_AusgabeString, "");
	startnummer++;
}

void start_und_zielnummer_ausgeben(int zielnummer, int startnummer){
	char tmp[4]; 
	itoa(zielnummer, tmp, 10);
	uart_puts(tmp);	
	uart_puts(" Ziel ");
	//uart_puts(tmp);	
	uart_puts("    ");
	itoa(startnummer, tmp, 10);
	uart_puts(tmp);
	uart_puts(" Start");
	uart_puts("\r\n");
}
		
 void interrupts_definieren(void)
 {
	DDRB &= ~(1 << PD2);  //Port D2 als Eingang
	PORTB |= (1 << PD2);	// Interner Pullupwiderstand fuer Port D2 aktiv        
	DDRB &= ~(1 << PD3);  //Port D3 als Eingang                          
	PORTB |= (1 << PD3);	// Interner Pullupwiderstand fuer Port D3 aktiv
	
	// Fallende Flanke der Lichtschranke generiert Interrupt bei INT0 und INT1		
	//* MCUCR &= ~(1 << ISC01 | 1 << ISC00 | 1 << ISC11 | 1 << ISC10);
	MCUCR |= (1 << ISC01 | 1 << ISC11 );
	MCUCR &= ~(1 << ISC00 | 1 << ISC10);
	GICR |= (1 << INT0 | 1 << INT1); // INT0 und INT1 aktivieren
	//	General Interrupt Control Register 

	GIFR |= (1 << INTF0 | 1 << INTF1); // Flag fuer INT0 und INT1 auf Null setzen
	// General Interrupt Flag Register

	// Timer 0 konfigurieren

	TCCR0 |= (1 << CS02 | 1 << CS00 | 1 << WGM01); // Prescaler 1024 und CTC Modus
	// ((14745600/1024)) = 14400
	OCR0 = 144 - 1;

	// Compare Interrupt erlauben
	TIMSK |= (1 << OCIE0);
	
	TIFR |= (1 << OCF0);
	// Global Interrupts aktivieren
	sei();
	
}


int main()
{
	/** aktuelle Zielnummer */
	unsigned int ui_Zielnummer_num =1 ;
	/** aktuelle Startnummer */
	unsigned int ui_Startnummer_num = 1;
	unsigned int ui_DelayZielManuell = 0;
	/**  ul_EndzeitpunktDelayStart definiert den Zeitpunkt bis zu dem die Messung am Start ausgesetzt wir */
	unsigned long ul_EndzeitpunktDelayStart = 0; 
	/**  ul_EndzeitpunktDelayZiel definiert den Zeitpunkt bis zu dem die Messung am Ziel ausgesetzt wir */
	unsigned long ul_EndzeitpunktDelayZiel = 0;
	interrupts_definieren();	
	uart_init();
	DDRD |= (1<<PD6)|(1<<PD7) ;
	/** Signalhorn deaktivieren, Port D6 auf Low setzen */
	SIGNALGEBER_START_AUSSCHALTEN();
	/** Signalhorn deaktivieren, Port D7 auf Low setzen */
	SIGNALGEBER_ZIEL_AUSSCHALTEN();

	uart_puts("Lichtschranke der Kanufreunde-Wiking Gladbeck e. V.\r\n");
	uart_puts("Version 1.0.3 vom 18.09.2020\r\n");
	while (1) 
	{
		//* Pruefen ob Delayende Start erreicht	
		if (vgul_HundertstelSekunden <= ul_EndzeitpunktDelayStart)	
		{
			//* Falls Delayende noch nicht erreicht, Interruptstatus auf NULL setzen
			gui_Status&=~INTERRUPT_START_ERKANNT;	
		}

		//* Pruefen ob Delayende Ziel erreicht	
		if (vgul_HundertstelSekunden <= ul_EndzeitpunktDelayZiel)
		{
			//* Falls Delayende noch nicht erreicht, Interruptstatus auf NULL setzen
			gui_Status&=~INTERRUPT_ZIEL_ERKANNT;
		}
		
		//if ((gui_Status&INTERRUPT_START_ERKANNT) & (vgul_HundertstelSekunden <= ul_EndzeitpunktDelayStart) ) 		
		if (gui_Status&INTERRUPT_START_ERKANNT ) 
		{
			gui_Status&=~INTERRUPT_START_ERKANNT;
			/** Signalhorn aktivieren, Port D6 auf High setzen */
			SIGNALGEBER_START_EINSCHALTEN();
			/** Abschaltzeitpunkt des Delays definieren  */
			ul_EndzeitpunktDelayStart = vgul_HundertstelSekunden + START_DELAY;
			/** Abschaltzeitpunkt des Signalhorns definieren  */
			ul_EndzeitpunktStartsignal = vgul_HundertstelSekunden + LAUTSPRECHER_DAUER_START;
			uart_puts(startnummer_ausgabe);
			zeit_ausgeben(vgul_StartHundertstelsekunden, "Start ");
		}
		
		if (gui_Status&INTERRUPT_ZIEL_ERKANNT)
		{
			gui_Status&=~INTERRUPT_ZIEL_ERKANNT;
			/** Signalhorn aktivieren, Port D4 auf High setzen */
			//PORTD &=~(1<<PD7 );
			SIGNALGEBER_ZIEL_EINSCHALTEN();
			/** Abschaltzeitpunkt des Delays definieren  */
			if (gui_Status&DELAY_ZIEL_ERKANNT)
			{
				ul_EndzeitpunktDelayZiel = vgul_HundertstelSekunden + ui_DelayZielManuell;
			
			}
			else
			{
				ul_EndzeitpunktDelayZiel = vgul_HundertstelSekunden + ZIEL_DELAY;
			}
			/** Abschaltzeitpunkt des Signalhorns definieren  */
			ul_EndzeitpunktZielsignal = vgul_HundertstelSekunden + LAUTSPRECHER_DAUER_ZIEL;
			/**   Naechste erwartete Zielnummer ausgeben */
			uart_puts(zielnummer_ausgabe);
			/** Zielzeit ausgeben  */
			zeit_ausgeben(vgul_ZielHundertstelsekunden, "Ziel                ");
		}
		 		
		/** Wenn Abschaltzeitpunkt fuer das Startsignal erreicht */
		 if(vgul_HundertstelSekunden> ul_EndzeitpunktStartsignal)
		 {
		 	//ul_EndzeitpunktStartsignal=0;
		 	/** Signalhorn deaktivieren, Port D6 auf Low setzen */
		 	SIGNALGEBER_START_AUSSCHALTEN();
		 } 
			 
		/** Wenn Abschaltzeitpunkt fuer das Zielsignal erreicht */
		if(vgul_HundertstelSekunden> ul_EndzeitpunktZielsignal)
			{
			//ul_EndzeitpunktZielsignal=0;
			/** Signalhorn deaktivieren, Port D7 auf Low setzen */
			
			SIGNALGEBER_ZIEL_AUSSCHALTEN();
			}
		
		/** Wenn die serielle Schnittstelle Daten geliefert hat */	
		if(uart_str_complete == 1)
		{	
			
			/**Ausgabestring kopieren */
			strcpy(erstes_zeichen, uart_string);
			/*				uart_puts(uart_string); */
			/** Alles bis auf das erste Zeichen loeschen */
			erstes_zeichen[1]='\0';
			/** Die einzelnen Faelle abarbeitetn */

			/*	uart_puts("\r\n"); */
			switch(*erstes_zeichen)
			{
					
				case 'z':
					/** falls erstes Zeichen = z */
					/** fuehrendes z entfernen und Rest in zielnummer_num abspeichern */
					strcpy(zielnummer_ausgabe, uart_string + 1);
					ui_Zielnummer_num = atoi(zielnummer_ausgabe);
					break;
					
				case 's':
					/** falls erstes Zeichen = s */
					// fuehrendes s entfernen und Rest in startnummer_num abspeichern
					strcpy(startnummer_ausgabe, uart_string + 1);
					ui_Startnummer_num = atoi(startnummer_ausgabe);
					break;	
					
				case 'y':		
					/** falls erstes Zeichen = y */
					/** Zielnummer um 1 erhoehen */
					ui_Zielnummer_num ++;
					itoa(ui_Zielnummer_num,zielnummer_ausgabe,10);
					break;
					
				case 'x':
					/** falls erstes Zeichen = x */
					ui_Startnummer_num ++;
					itoa(ui_Startnummer_num,startnummer_ausgabe,10);
					break;
					
				case 'n':
				/** falls erstes Zeichen = n */
					ui_Zielnummer_num +=3;
					itoa(ui_Zielnummer_num,zielnummer_ausgabe,10);
					break;
					
				case 'm':
					/** falls erstes Zeichen = m */
					ui_Startnummer_num +=3;
					itoa(ui_Startnummer_num,startnummer_ausgabe,10);
					break;
					
				case 'q':
					/** falls erstes Zeichen = q */
					ui_Zielnummer_num --;
					itoa(ui_Zielnummer_num,zielnummer_ausgabe,10);
					break;
					
				case 'w':
					/** falls erstes Zeichen = w */
					ui_Startnummer_num --;
					itoa(ui_Startnummer_num,startnummer_ausgabe,10);
					break;
					
				case 'o':
					/** falls erstes Zeichen = o */
					ui_Zielnummer_num -=3;
					itoa(ui_Zielnummer_num,zielnummer_ausgabe,10);
					break;
					
				case 'p':
					/** falls erstes Zeichen = p */	
					ui_Startnummer_num -=3;
					itoa(ui_Startnummer_num,startnummer_ausgabe,10);
					break;

				case 'h':
				/** falls erstes Zeichen = h */
				uart_puts("Hilfe fuer die Lichtschranke\r\n");
				uart_puts("\r\n");
				uart_puts("z100 ENTER setzt die 100 fuer den Zieldurchlauf\r\n");
				uart_puts("s133 ENTER setzt die 133 fuer den Stardurchlauf\r\n");
				uart_puts("\r\n");
				uart_puts("Die Tasten q w y x sind fuer die Einzelwettkaempfe\r\n");
				uart_puts("MERKHILFE: die linkte Taste ist immer fuer das Ziel, da das Ziel bei uns links liegt\r\n");
				uart_puts("\r\n");
				uart_puts("q ENTER erniedrigt die Zielnummer um 1\r\n");
				uart_puts("w ENTER erniedrigt die Startnummer um 1\r\n");				
				uart_puts("y ENTER erhoeht die Zielnummer um 1\r\n");
				uart_puts("x ENTER erhoeht die Startnummer um 1\r\n");
				uart_puts("\r\n");
				uart_puts("Die Tasten o p n m sind fuer die Manschaftswettkaempfe\r\n");
				uart_puts("\r\n");
				uart_puts("o ENTER erniedrigt die Zielnummer um 3\r\n");
				uart_puts("p ENTER erniedrigt die Startnummer um 3\r\n");
				uart_puts("n ENTER erhoeht die Zielnummer um 3\r\n");
				uart_puts("m ENTER erhoeht die Startnummer um 3\r\n");
				uart_puts("\r\n");
				uart_puts("\r\n");
				uart_puts("h ENTER zeigt die Hilfe an \r\n");
				uart_puts("\r\n");
				uart_puts("\r\n");
				
				break;
				
				case 'd':
			    /** falls erstes Zeichen = d 
				   fuehrendes d entfernen */
					strcpy(tmp_offset, uart_string + 1);
					ui_DelayZielManuell = atoi(tmp_offset);
					if(ui_DelayZielManuell > 50)
					{
						uart_puts("Delay muss zwischen 0 und 50 betragen \r\n");
						ui_DelayZielManuell = 0;
					}
					else
					{
						gui_Status|=DELAY_ZIEL_ERKANNT;
						uart_puts("Dealy Ziel ");	
						uart_puts(tmp_offset);
						uart_puts("\r\n");
					}
					break;				
					 
				case 't':
			    /** falls erstes Zeichen = t 
				   fuehrendes t entfernen */
					strcpy(tmp_offset, uart_string + 1);
					vgul_HundertstelSekunden = atol(tmp_offset);
					zeit_ausgeben(vgul_HundertstelSekunden, "Uhrzeit  ");
					break;
			}
			start_und_zielnummer_ausgeben(ui_Zielnummer_num, ui_Startnummer_num);
			uart_str_complete = 0;
		}
	}
}



